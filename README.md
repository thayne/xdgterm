# xdgterm

Script to launch the user's preferred terminal using the XDG terminal intent (https://zbrown.pages.freedesktop.org/term-rendered/)